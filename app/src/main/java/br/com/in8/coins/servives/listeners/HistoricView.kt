package br.com.in8.coins.servives.listeners

import br.com.in8.coins.model.Historic
import br.com.in8.coins.model.enums.ErrorType

interface HistoricView {
    fun onSuccess(data: List<Historic>)
    fun onError(error: ErrorType)
}