package br.com.in8.coins.fragments

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.ViewModelProviders
import br.com.in8.coins.R
import br.com.in8.coins.databinding.FragmentConversionBinding
import br.com.in8.coins.ui.managers.CustomDialogManager
import br.com.in8.coins.ui.managers.FindCoinDialogManager
import br.com.in8.coins.viewmodels.fragments.ConversionViewModel
import br.com.in8.coins.viewmodels.states.ConversionState
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.runOnUiThread
import java.lang.ref.WeakReference

class ConversionFragment : MainBaseFragment() {

    private lateinit var binding: FragmentConversionBinding
    private val customDialogManager: CustomDialogManager by lazy {
        CustomDialogManager(
            requireContext()
        )
    }

    private val viewModel: ConversionViewModel by lazy {
        ViewModelProviders.of(this).get(ConversionViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_conversion, container, false)
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        actionListener?.let {
            it.backButtonEnable(false)
            it.setToolbarVisibility(true)
            it.toggleHomeButton(false)
            it.toogleToolbarLogo(true)
            it.toogleNavigationBar(false)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_home, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        context?.let { context ->
            viewModel.state.addOnPropertyChangedCallback(StateCallback(this))

            binding.btnConversion.setOnClickListener {
                viewModel.conversion()
            }

            binding.coinTypeBefore.setOnClickListener {
                FindCoinDialogManager(requireContext()).show2 {
                    onFindCoinType {
                        viewModel.cointypeBefore.set(it)
                    }
                }
            }

            binding.coinTypeAfter.setOnClickListener {
                FindCoinDialogManager(context).show2 {
                    onFindCoinType {
                        viewModel.cointypeAfter.set(it)
                    }
                }
            }
        }
    }

    private fun handleState() {
        when (viewModel.state.get()) {
            ConversionState.ERROR_INPUT -> {
                customDialogManager.dismiss()
                mainActivity?.parentView?.snackbar(R.string.error_input_text)
            }
            ConversionState.ERROR_INTERNET -> {
                customDialogManager.dismiss()
                mainActivity?.parentView?.snackbar(R.string.error_text_intenet)
            }
            ConversionState.SUCCESS -> customDialogManager.dismiss()
            ConversionState.PROCESS -> customDialogManager.showLoading("Processando conversão...")
            ConversionState.ERROR_SERVICE -> {
                customDialogManager.dismiss()
                mainActivity?.parentView?.snackbar(R.string.error_text)
            }
        }
    }


    //region ~~~~ StateCallback ~~~~
    private class StateCallback(view: ConversionFragment) : Observable.OnPropertyChangedCallback() {
        private val context = WeakReference<ConversionFragment>(view)

        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            context.get()?.let {
                if (it.isAdded) {
                    it.runOnUiThread { it.handleState() }
                }
            }
        }
    }
    //endregion
}