package br.com.in8.coins.fragments

import br.com.in8.coins.activities.MainActivity

open class MainBaseFragment : BaseFragment() {

    protected val mainActivity: MainActivity? by lazy { activity as? MainActivity }

}
