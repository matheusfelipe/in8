package br.com.in8.coins.application

import android.app.Application
import br.com.in8.coins.R
import br.com.in8.coins.di.uiModule
import br.com.in8.coins.utils.Reachability
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import org.koin.android.ext.koin.with
import org.koin.java.standalone.KoinJavaStarter
import org.koin.standalone.StandAloneContext

class CoinApp : Application() {
    companion object {
        lateinit var instance: CoinApp
            private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        //Koin Modules
        KoinJavaStarter.startKoin(listOf(uiModule)) with this

        //Callygraphy
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build())
                ).build())

        //Reachability
        Reachability.registerReachability()

        //DBFlow
        FlowManager.init(FlowConfig.Builder(this).openDatabasesOnInit(true).build())

    }

    override fun onTerminate() {
        super.onTerminate()

        StandAloneContext.stopKoin()
    }

}