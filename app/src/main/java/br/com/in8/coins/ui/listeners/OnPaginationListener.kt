package br.com.in8.coins.ui.listeners

interface OnPaginationListener {
    fun isLastPage(): Boolean
    fun isLoading(): Boolean
    fun loadMoreItems()
}
