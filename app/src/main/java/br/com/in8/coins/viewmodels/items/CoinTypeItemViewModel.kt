package br.com.in8.coins.viewmodels.items


class CoinTypeItemViewModel(
    val id: Long = 0,
    val name: String = "",
    var isCheck: Boolean = false
) {

    companion object {
        fun fromCoinType(name: String): CoinTypeItemViewModel =
            CoinTypeItemViewModel(0, name, false)
    }

    override operator fun equals(other: Any?): Boolean {
        if (other == null || other !is CoinTypeItemViewModel) {
            return false
        }

        return id == other.id
    }

    override fun hashCode(): Int = id.hashCode()

}