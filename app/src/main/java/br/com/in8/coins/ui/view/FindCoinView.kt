package br.com.in8.coins.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.in8.coins.R
import br.com.in8.coins.adapters.CoinTypeAdapter
import br.com.in8.coins.databinding.ViewFindCoinBinding
import br.com.in8.coins.ui.listeners.OnFindCoinListener
import br.com.in8.coins.ui.listeners.OnRecyclerViewClickListener
import br.com.in8.coins.ui.listeners.defaultFindCoinListener
import br.com.in8.coins.viewmodels.items.CoinTypeItemViewModel
import br.com.in8.coins.viewmodels.views.FindCoinViewModel
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import org.jetbrains.anko.layoutInflater

class FindCoinView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr),
    OnRecyclerViewClickListener<CoinTypeItemViewModel> {

    companion object {
        fun openAtDialog(
            context: Context,
            listener: OnFindCoinListener
        ): MaterialDialog {
            val view = context.layoutInflater.inflate(R.layout.view_find_coin, null) as FindCoinView

            view.listener = listener
            view.initViewComponents()

            return MaterialDialog(context).show {
                customView(view = view, scrollable = false, noVerticalPadding = true)
                cancelOnTouchOutside(false)
                cancelable(false)
            }
        }
    }

    private val adapter: CoinTypeAdapter by lazy { CoinTypeAdapter(this) }

    private lateinit var binding: ViewFindCoinBinding

    private var listener: OnFindCoinListener = defaultFindCoinListener()

    private val viewModel: FindCoinViewModel by lazy {
        FindCoinViewModel()
    }

    private fun initViewComponents() {
        (DataBindingUtil.bind(this) as? ViewFindCoinBinding)?.let { bng ->
            binding = bng

            binding.viewModel = viewModel
            binding.recyclerCoinType.adapter = adapter
            binding.recyclerCoinType.layoutManager =
                LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            binding.imageButtonClose.setOnClickListener {
                listener.onClose()
            }
            binding.btnNext.setOnClickListener {
                listener.onSave(adapter.getSelectedItem().name)
            }

            viewModel.search(adapter)


        } ?: run {
            listener.onError()
        }
    }

    override fun onItemSelected(item: CoinTypeItemViewModel, position: Int) {
        viewModel.selectItem(item, position)
    }

}
