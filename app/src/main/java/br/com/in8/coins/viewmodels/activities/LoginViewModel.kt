package br.com.in8.coins.viewmodels.activities

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import br.com.in8.coins.application.CoinApp
import br.com.in8.coins.model.enums.ErrorType
import br.com.in8.coins.preferences.EntitiesPreferences
import br.com.in8.coins.servives.UserService
import br.com.in8.coins.servives.listeners.LoginView
import br.com.in8.coins.viewmodels.states.ConversionState
import br.com.in8.coins.viewmodels.states.LoginState
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class LoginViewModel : ViewModel(), KoinComponent, LoginView {

    val username = ObservableField<String>()
    val password = ObservableField<String>()
    val state = ObservableField<LoginState>()

    private val entitiesPreferences: EntitiesPreferences by inject()
    private val userService: UserService by lazy { UserService() }

    fun login() {
        state.set(LoginState.PROCCESS)
        userService.login(username.get().toString(), password.get().toString(), this)
    }

    override fun onSuccess() {
        state.set(LoginState.SUCCESS)
    }

    override fun onError(error: ErrorType) {
        state.set(LoginState.ERROR)
    }

    fun check() {
        if (entitiesPreferences.isCurrentUser(CoinApp.instance)) {
            state.set(LoginState.SUCCESS)
        }
    }
}