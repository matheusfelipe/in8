package br.com.in8.coins.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.ViewModelProviders
import br.com.in8.coins.R
import br.com.in8.coins.databinding.ActivityLoginBinding
import br.com.in8.coins.fragments.ConversionFragment
import br.com.in8.coins.ui.managers.CustomDialogManager
import br.com.in8.coins.viewmodels.activities.LoginViewModel
import br.com.in8.coins.viewmodels.states.LoginState
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.support.v4.runOnUiThread
import org.koin.standalone.KoinComponent
import java.lang.ref.WeakReference

class LoginActivity : BaseActivity(), KoinComponent {

    private val customDialogManager: CustomDialogManager by lazy {
        CustomDialogManager(this)
    }

    private val loginModel: LoginViewModel by lazy {
        ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }

    private val binding: ActivityLoginBinding by lazy {
        DataBindingUtil.setContentView(this, R.layout.activity_login) as ActivityLoginBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.viewModel = loginModel
        loginModel.state.addOnPropertyChangedCallback(StateCallback(this))
        binding.btnLogin.setOnClickListener {
            loginModel.login()
        }
        loginModel.check()
    }

    private fun handleAuthState() {
        when(loginModel.state.get()) {
            LoginState.PROCCESS -> customDialogManager.showLoading("Carregando...")
            LoginState.SUCCESS -> {
                customDialogManager.dismiss()
                startMain()
            }
            LoginState.ERROR -> {
                customDialogManager.dismiss()
                parentView?.snackbar(R.string.error_login)
            }
        }
    }

    private fun startMain() {
        startActivity(intentFor<MainActivity>())
    }

    //region ~~~~ StateCallback ~~~~
    private class StateCallback(context: LoginActivity) : Observable.OnPropertyChangedCallback() {
        private val localInstance = WeakReference(context)

        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            localInstance.get()?.runOnUiThread {
                localInstance.get()?.handleAuthState()
            }
        }
    }
    //endregion
}