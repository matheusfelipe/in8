package br.com.in8.coins.servives

import android.annotation.SuppressLint
import br.com.in8.coins.application.CoinApp
import br.com.in8.coins.model.Historic
import br.com.in8.coins.model.enums.ErrorType
import br.com.in8.coins.persistence.HistoricRepository
import br.com.in8.coins.preferences.EntitiesPreferences
import br.com.in8.coins.servives.listeners.HistoricView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

@SuppressLint("CheckResult")
class HistoricService : KoinComponent, AnkoLogger {

    private val entitiesPreferences: EntitiesPreferences by inject()
    private val historicRepository: HistoricRepository by lazy { HistoricRepository() }

    fun getAll(callback: HistoricView) {
        Observable.fromCallable {
            val user = entitiesPreferences.appUser(CoinApp.instance)
            historicRepository.findByUser(user.id) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess(it)
            }, {
                error(it.message, it)
                callback.onError(ErrorType.DATABASE)
            })
    }



    fun save(historic: Historic): Historic {
        if (historicRepository.save(historic)) return historic

        throw IllegalStateException()
    }
}