package br.com.in8.coins.viewmodels.activities

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import br.com.in8.coins.application.CoinApp
import br.com.in8.coins.preferences.EntitiesPreferences
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class MainViewModel : ViewModel(), KoinComponent {

    private val entitiesPreferences: EntitiesPreferences by inject()

    fun clean() {
        entitiesPreferences.cleanUser(CoinApp.instance)
    }

    //region ~~~~ Factory ~~~~
    class Factory(
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = MainViewModel() as T
    }
    //endregion
}
