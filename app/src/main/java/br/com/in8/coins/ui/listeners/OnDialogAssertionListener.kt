package br.com.in8.coins.ui.listeners

interface OnDialogAssertionListener {
    fun onClose()
    fun onSucceed()
    fun onError()
}