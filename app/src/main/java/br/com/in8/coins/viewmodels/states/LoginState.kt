package br.com.in8.coins.viewmodels.states

enum class LoginState {
    PROCCESS, SUCCESS, ERROR
}