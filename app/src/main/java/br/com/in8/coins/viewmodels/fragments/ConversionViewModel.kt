package br.com.in8.coins.viewmodels.fragments

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import br.com.in8.coins.servives.CoinService
import br.com.in8.coins.viewmodels.states.ConversionState
import org.koin.standalone.KoinComponent

class ConversionViewModel : ViewModel(), KoinComponent {

    private val coinService: CoinService by lazy { CoinService() }
    var cointypeAfter = ObservableField<String>()
    var cointypeBefore = ObservableField<String>()
    var result = ObservableField<String>()
    var coinValue = ObservableField<String>()
    val state = ObservableField<ConversionState>()

    fun conversion() {
        state.set(ConversionState.PROCESS)
        if (cointypeAfter.get().isNullOrEmpty() || cointypeBefore.get().isNullOrEmpty() ||
            coinValue.get().isNullOrEmpty()) {
            state.set(ConversionState.ERROR_INPUT)
        } else {
            coinService.conversionCoin(cointypeBefore.get().toString(), cointypeAfter.get().toString(),
                coinValue.get()?.toFloat() ?: 0F  , {
                    state.set(ConversionState.SUCCESS)
                    result.set("Resultado: $it")
            },{
                    result.set("")
                    state.set(ConversionState.ERROR_SERVICE)
            })
        }

    }

}