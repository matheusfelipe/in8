package br.com.in8.coins.viewmodels.states

enum class ConversionState {
    ERROR_INPUT, ERROR_INTERNET, SUCCESS, PROCESS, ERROR_SERVICE
}