package br.com.in8.coins.ui.listeners

fun defaultFindCoinListener(): OnFindCoinListener =
        object : OnFindCoinListener {

            override fun onSave(data: String) {
            }

            override fun onClose() {
            }

            override fun onSucceed() {
            }

            override fun onError() {
            }
        }

