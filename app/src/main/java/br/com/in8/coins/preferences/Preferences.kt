package br.com.in8.coins.preferences

import android.content.Context
import android.content.SharedPreferences

private const val PREFERENCE_REF = "coin_pref"

private fun preferences(context: Context): SharedPreferences = context.getSharedPreferences(PREFERENCE_REF, Context.MODE_PRIVATE)

fun save(context: Context, key: String, value: String): Boolean =
        preferences(context)
                .edit()
                .putString(key, value)
                .commit()

fun save(context: Context, key: String, value: Boolean): Boolean =
        preferences(context)
                .edit()
                .putBoolean(key, value)
                .commit()

fun save(context: Context, key: String, value: Int): Boolean =
        preferences(context)
                .edit()
                .putInt(key, value)
                .commit()

fun save(context: Context, key: String, value: Set<String>): Boolean =
        preferences(context)
                .edit()
                .putStringSet(key, value)
                .commit()

fun fetchString(context: Context, key: String, defaultValue: String = ""): String? =
        preferences(context)
                .getString(key, defaultValue)

fun fetchBoolean(context: Context, key: String, defaultValue: Boolean = false): Boolean =
        preferences(context)
                .getBoolean(key, defaultValue)

fun fetchInt(context: Context, key: String, defaultValue: Int = 0): Int =
        preferences(context)
                .getInt(key, defaultValue)

fun fetchSet(context: Context, key: String): Set<String>? =
        preferences(context)
                .getStringSet(key, setOf())

fun remove(context: Context, key: String): Boolean =
        preferences(context)
                .edit()
                .remove(key)
                .commit()
