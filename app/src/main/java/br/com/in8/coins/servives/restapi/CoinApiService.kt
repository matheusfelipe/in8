package br.com.in8.coins.servives.restapi

import android.annotation.SuppressLint
import br.com.in8.coins.model.Coin
import br.com.in8.coins.model.enums.ErrorType
import br.com.in8.coins.servives.listeners.CoinView
import br.com.in8.coins.servives.rest.CoinRest
import br.com.in8.coins.utils.Reachability
import br.com.in8.coins.utils.SchedulerUtils
import io.reactivex.Observable
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error

@SuppressLint("CheckResult")
class CoinApiService : AnkoLogger {

    private val service: CoinRest by lazy { NetworkModule.createService(CoinRest::class.java) }

    fun searchConversion(base: Map<String,String>, callback: CoinView) {
        if (Reachability.isReachable) {
            conversionRequest(base)
                .observeOn(SchedulerUtils.observeOn())
                .subscribeOn(SchedulerUtils.subscribeOn())
                .subscribe({
                    callback.onSuccess(it)
                }, { e ->
                    error(e.message, e)
                    callback.onError(ErrorType.SERVICE_COIN)
                })
        } else {
            callback.onError(ErrorType.SERVICE_INTERNET)
        }
    }

    private fun conversionRequest(base: Map<String,String>): Observable<Coin> =
        service.getCoins(base)

}