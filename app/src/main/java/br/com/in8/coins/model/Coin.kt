package br.com.in8.coins.model

import java.io.Serializable

class Coin(
    var base: String = "",
    var date: String = "",
    var rates: Map<String, Double> = mapOf()
) : Serializable {

    companion object {
        const val serialVersionUID = 1L
    }


}