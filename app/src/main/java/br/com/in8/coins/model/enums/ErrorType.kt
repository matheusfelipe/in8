package br.com.in8.coins.model.enums

enum class ErrorType {
    DEFAULT,
    SERVICE,
    SEARCH,
    SERVICE_INTERNET,
    NOT_FOUND,
    SERVICE_COIN,
    DATABASE
}