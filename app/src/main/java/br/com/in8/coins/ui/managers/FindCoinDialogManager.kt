package br.com.in8.coins.ui.managers

import android.content.Context
import br.com.in8.coins.ui.listeners.OnFindCoinListener
import br.com.in8.coins.ui.view.FindCoinView

class FindCoinDialogManager (
    private val context: Context
) : AssertionDialogManager(), OnFindCoinListener {

    private var coinCallback: (String) -> Unit = { }

    override fun open() {
        dialog = FindCoinView.openAtDialog(context, this)
    }

    inline fun show2(func: FindCoinDialogManager.() -> Unit): FindCoinDialogManager {
        this.func()
        this.open()
        return this
    }

    fun onFindCoinType(callback: (String) -> Unit) {
        coinCallback = callback
    }

    //region ~~~~ OnFindCoinListener ~~~~
    override fun onSave(data: String) {
        onClose()
        coinCallback.invoke(data)
    }
    //endregion
}