package br.com.in8.coins.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.in8.coins.databinding.ItemHistoricBinding
import br.com.in8.coins.ui.listeners.OnRecyclerViewClickListener
import br.com.in8.coins.viewmodels.items.HistoricItemViewModel
import org.jetbrains.anko.layoutInflater

class HistoricAdapter(
    private val clickListener: OnRecyclerViewClickListener<HistoricItemViewModel>
) : RecyclerView.Adapter<HistoricAdapter.ViewHolder>() {

    private val items = ArrayList<HistoricItemViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemHistoricBinding.inflate(parent.context.layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], position)

    fun updateItems(data: List<HistoricItemViewModel>) {
        items.clear()
        items.addAll(data)

        notifyDataSetChanged()
    }

    fun update(newItems: List<HistoricItemViewModel>, clearData: Boolean = false) {
        if (clearData) {
            items.clear()
        }
        items.addAll(newItems)
        notifyDataSetChanged()
    }


    //region ~~~~ ViewHolder ~~~~
    inner class ViewHolder(val binding: ItemHistoricBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: HistoricItemViewModel, position: Int) {
            binding.item = item
            binding.viewContainerItem.setOnClickListener {
                clickListener.onItemSelected(
                    item,
                    position
                )
            }
            binding.executePendingBindings()
        }
    }
    //endregion

}
