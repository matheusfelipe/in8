package br.com.in8.coins.fragments

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.in8.coins.R
import br.com.in8.coins.adapters.HistoricAdapter
import br.com.in8.coins.databinding.FragmentHistoricBinding
import br.com.in8.coins.ui.listeners.OnRecyclerViewClickListener
import br.com.in8.coins.ui.listeners.PaginationScrollListener
import br.com.in8.coins.ui.managers.CustomDialogManager
import br.com.in8.coins.viewmodels.fragments.HistoricViewModel
import br.com.in8.coins.viewmodels.items.CoinTypeItemViewModel
import br.com.in8.coins.viewmodels.items.HistoricItemViewModel
import org.jetbrains.anko.support.v4.onRefresh

class HistoricFragment : MainBaseFragment(), OnRecyclerViewClickListener<HistoricItemViewModel> {

    private lateinit var binding: FragmentHistoricBinding
    private val customDialogManager: CustomDialogManager by lazy {
        CustomDialogManager(
            requireContext()
        )
    }

    private val adapter: HistoricAdapter by lazy { HistoricAdapter(this) }

    private val viewModel: HistoricViewModel by lazy {
        ViewModelProviders.of(this, HistoricViewModel.Factory(adapter)).get(HistoricViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_historic, container, false)
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        actionListener?.let {
            it.setToolbarVisibility(false)
            it.backButtonEnable(true)
            it.toogleNavigationBar(true)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(false)

        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.imageButtonClose.setOnClickListener { dismiss() }
        binding.recyclerHistories.adapter = adapter
        binding.recyclerHistories.layoutManager = layoutManager

        binding.swipeHistories.onRefresh {
            viewModel.listAll()
            binding.swipeHistories.isRefreshing = false
        }
        viewModel.listAll()
    }


    override fun onItemSelected(item: HistoricItemViewModel, position: Int) {

    }
}