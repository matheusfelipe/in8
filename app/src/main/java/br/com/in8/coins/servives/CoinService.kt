package br.com.in8.coins.servives

import br.com.in8.coins.application.CoinApp
import br.com.in8.coins.model.Coin
import br.com.in8.coins.model.Historic
import br.com.in8.coins.model.enums.ErrorType
import br.com.in8.coins.preferences.EntitiesPreferences
import br.com.in8.coins.servives.listeners.CoinView
import br.com.in8.coins.servives.restapi.CoinApiService
import org.jetbrains.anko.AnkoLogger
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.util.*

class CoinService : KoinComponent, AnkoLogger {

    private val coinApiService: CoinApiService by lazy { CoinApiService() }
    private val historicservice: HistoricService by lazy { HistoricService() }
    private val entitiesPreferences: EntitiesPreferences by inject()

    fun conversion(base: String, view: CoinView) {
        coinApiService.searchConversion(hashMapOf("base" to base), view)
    }

    fun conversionCoin(
        before: String, after: String, value: Float, success: (Double) -> Unit,
        error: (ErrorType) -> Unit
    ) {
        val user = entitiesPreferences.appUser(CoinApp.instance)

        conversion(before, object : CoinView {
            override fun onSuccess(data: Coin) {
                if (data.rates.containsKey(after)  ){
                    data.rates.get(after)?.let { coin ->
                        val result = coin * value
                        val historic = Historic(UUID.randomUUID().toString(),before, after, value.toString(),
                            result.toString(), data.date, user.id)
                        historicservice.save(historic)
                        success(coin * value)
                    } ?: run {
                        error(ErrorType.NOT_FOUND)
                    }
                } else {
                    error(ErrorType.NOT_FOUND)
                }
            }

            override fun onError(e: ErrorType) {
                error(e)
            }

        })
    }

}