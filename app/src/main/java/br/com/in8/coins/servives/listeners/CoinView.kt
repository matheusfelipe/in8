package br.com.in8.coins.servives.listeners

import br.com.in8.coins.model.Coin
import br.com.in8.coins.model.enums.ErrorType

interface CoinView {
    fun onSuccess(data: Coin)
    fun onError(error: ErrorType)
}