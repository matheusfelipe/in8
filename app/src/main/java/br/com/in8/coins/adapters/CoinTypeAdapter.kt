package br.com.in8.coins.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.in8.coins.databinding.ItemCoinTypeBinding
import br.com.in8.coins.ui.listeners.OnRecyclerViewClickListener
import br.com.in8.coins.viewmodels.items.CoinTypeItemViewModel
import org.jetbrains.anko.layoutInflater

class CoinTypeAdapter(
    private val clickListener: OnRecyclerViewClickListener<CoinTypeItemViewModel>
) : RecyclerView.Adapter<CoinTypeAdapter.ViewHolder>() {

    private val items = ArrayList<CoinTypeItemViewModel>()
    private var selectedItem: CoinTypeItemViewModel? = null
    private var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCoinTypeBinding.inflate(parent.context.layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], position)

    fun updateItems(data: List<CoinTypeItemViewModel>) {
        items.clear()
        items.addAll(data)

        selectedItem?.let {
            selectedPosition = 0

            items.remove(it)
            items.add(0, it)
        }

        notifyDataSetChanged()
    }

    fun updateSelectedItem(item: CoinTypeItemViewModel, position: Int) {
        getSelectedItem().isCheck = false

        selectedItem = item
        selectedPosition = position

        notifyDataSetChanged()
    }

    fun getSelectedItem() = items.getOrElse(selectedPosition) { _ -> CoinTypeItemViewModel() }

    //region ~~~~ ViewHolder ~~~~
    inner class ViewHolder(val binding: ItemCoinTypeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CoinTypeItemViewModel, position: Int) {
            binding.item = item
            binding.viewContainerItem.setOnClickListener { clickListener.onItemSelected(item, position) }
            binding.executePendingBindings()
        }
    }
    //endregion

}
