package br.com.in8.coins.viewmodels.items

import br.com.in8.coins.model.Historic
import br.com.in8.coins.utils.DateUtils
import java.util.*

class HistoricItemViewModel(
    val id: String = "",
    val coinBefore: String = "",
    val coinAfter: String = "",
    val date: String = "",
    val valueBefore: String = "",
    val valueAfter: String = ""
) {

    companion object {
        fun fromHistoric(historic: Historic): HistoricItemViewModel =
            HistoricItemViewModel(
                historic.id, historic.coinBefore, historic.coinAfter, historic.date,
                historic.valueBefore, historic.valueAfter
            )
    }

    fun full() = "$coinBefore ($valueBefore) --> $coinAfter ($valueAfter)"

    fun data() =
        DateUtils.getDatePtBR(Date(date.replace("-", "/")))


    override operator fun equals(other: Any?): Boolean {
        if (other == null || other !is HistoricItemViewModel) {
            return false
        }

        return id == other.id
    }

    override fun hashCode(): Int = id.hashCode()
}
