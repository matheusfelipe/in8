package br.com.in8.coins.ui.listeners

interface OnRecyclerViewClickListener<T> {
    fun onItemSelected(item: T, position: Int)
}
