package br.com.in8.coins.model

import br.com.in8.coins.persistence.AppDatabase
import com.raizlabs.android.dbflow.annotation.ConflictAction
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import org.parceler.Parcel
import java.io.Serializable

@Parcel
@Table(name = "historic", database = AppDatabase::class, allFields = true, insertConflict = ConflictAction.REPLACE)
data class Historic(
    @PrimaryKey
    var id: String = "",
    var coinBefore: String = "",
    var coinAfter: String = "",
    var valueBefore: String = "",
    var valueAfter: String = "",
    var date: String = "",
    var user: String = ""
    ): Serializable {

    companion object {
        const val serialVersionUID = 1L
    }

}