package br.com.in8.coins.persistence

import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.sql.language.Method
import com.raizlabs.android.dbflow.sql.language.OrderBy
import com.raizlabs.android.dbflow.sql.language.SQLOperator
import com.raizlabs.android.dbflow.sql.language.Select
import com.raizlabs.android.dbflow.structure.ModelAdapter
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import java.io.IOException

abstract class RepositoryBase<T>(
        private val clazz: Class<T>
) : AnkoLogger {

    companion object {
        const val DEFAULT_RESULT_OFFSET = 50
    }

    protected val db: ModelAdapter<T> by lazy { FlowManager.getModelAdapter(clazz) }

    fun executeTransaction(transaction: ITransaction): Boolean {
        var successful = false

        return try {
            FlowManager.getDatabase(AppDatabase::class.java).beginTransactionAsync(transaction)
                    .runCallbacksOnSameThread(true)
                    .success {
                        successful = true
                    }
                    .error { _, throwable ->
                        successful = false
                        error(throwable.message, throwable)
                    }
                    .build()
                    .executeSync()

            successful
        } catch (e: IOException) {
            error(e.message, e)
            false
        }
    }

    fun exists(model: T): Boolean = db.exists(model)

    fun count(): Long =
            Select(Method.count())
                    .from(clazz)
                    .longValue()

    fun count(vararg conditions: SQLOperator): Long =
            Select(Method.count())
                    .from(clazz)
                    .where(*conditions)
                    .longValue()

    fun find(vararg conditions: SQLOperator): T? =
            Select()
                    .from(clazz)
                    .where(*conditions)
                    .querySingle()

    fun findAll(): List<T> =
            Select()
                    .from(clazz)
                    .queryList()

    fun findAll(page: Int, offset: Int = DEFAULT_RESULT_OFFSET, ordering: OrderBy): List<T> =
            Select()
                    .from(clazz)
                    .limit(offset)
                    .offset(page * offset)
                    .orderBy(ordering)
                    .queryList()

    fun findAll(page: Int, offset: Int = DEFAULT_RESULT_OFFSET, vararg conditions: SQLOperator): List<T> =
            Select()
                    .from(clazz)
                    .where(*conditions)
                    .limit(offset)
                    .offset(page * offset)
                    .queryList()

    fun findAll(page: Int, offset: Int = DEFAULT_RESULT_OFFSET, ordering: OrderBy,
                vararg conditions: SQLOperator): List<T> =
            Select()
                    .from(clazz)
                    .where(*conditions)
                    .limit(offset)
                    .offset(page * offset)
                    .orderBy(ordering)
                    .queryList()


    fun findAll(vararg conditions: SQLOperator): List<T> =
            Select()
                    .from(clazz)
                    .where(*conditions)
                    .queryList()

    //region ~~~~ Open Interface ~~~~
    open fun save(model: T): Boolean {
        isValid(model)
        return db.save(model)
    }

    open fun update(model: T): Boolean {
        isValid(model)
        return db.update(model)
    }
    //endregion

    //region ~~~~ Abstract Interface ~~~~
    abstract fun isValid(model: T)
    //endregion

}
