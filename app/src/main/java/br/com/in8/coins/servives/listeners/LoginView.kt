package br.com.in8.coins.servives.listeners

import br.com.in8.coins.model.enums.ErrorType

interface LoginView {
    fun onSuccess()
    fun onError(error: ErrorType)
}