package br.com.in8.coins.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import br.com.in8.coins.application.CoinApp

object Reachability {
    var isReachable = false

    private var isReceiving = false
    private var receiver: BroadcastReceiver? = null

    fun registerReachability(): Boolean {
        val connectivityManager = CoinApp.instance
                .getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager

        if (isReceiving) {
            CoinApp.instance.unregisterReceiver(receiver)
            isReceiving = false
        }

        val info = connectivityManager?.activeNetworkInfo
        isReachable = info != null && info.isConnected

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val manager = context
                        .getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
                val netInfo = manager?.activeNetworkInfo
                isReachable = netInfo != null && netInfo.isConnected
            }

        }
        CoinApp.instance.registerReceiver(
            receiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        isReceiving = true

        return isReachable
    }
}
