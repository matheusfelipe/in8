package br.com.in8.coins.utils

import org.jetbrains.anko.AnkoLogger
import java.text.SimpleDateFormat
import java.util.Date

object DateUtils : AnkoLogger {

    fun getDatePtBR(date: Date): String {
        val outFormat = SimpleDateFormat("dd/MM/yyyy")
        return outFormat.format(date)
    }

}
