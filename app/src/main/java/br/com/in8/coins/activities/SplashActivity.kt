package br.com.in8.coins.activities

import android.os.Bundle
import br.com.in8.coins.R
import org.jetbrains.anko.intentFor

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (isTaskRoot) {
            setContentView(R.layout.activity_splash)
            startActivity(intentFor<LoginActivity>())
        }

        finish()
    }
}
