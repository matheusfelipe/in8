package br.com.in8.coins.viewmodels.fragments

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import br.com.in8.coins.adapters.HistoricAdapter
import br.com.in8.coins.model.Historic
import br.com.in8.coins.model.enums.ErrorType
import br.com.in8.coins.servives.HistoricService
import br.com.in8.coins.servives.listeners.HistoricView
import br.com.in8.coins.viewmodels.items.HistoricItemViewModel

class HistoricViewModel(
    private val adapter: HistoricAdapter
) : ViewModel(), HistoricView {

    private var currentPage = 1

    private val service: HistoricService by lazy { HistoricService() }

    fun listAll() {
        currentPage = 1
        service.getAll(this)
    }

    override fun onSuccess(data: List<Historic>) {
        update(data, true)
    }

    override fun onError(error: ErrorType) {

    }

    private fun update(items: List<Historic>, clear: Boolean) {
        adapter.update(items.map { ev -> HistoricItemViewModel.fromHistoric(ev) }, clear)
    }

    //region ~~~~ Factory ~~~~
    class Factory(
        private val adapter: HistoricAdapter
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = HistoricViewModel(adapter) as T
    }


}