package br.com.in8.coins.servives

import android.annotation.SuppressLint
import br.com.in8.coins.application.CoinApp
import br.com.in8.coins.model.User
import br.com.in8.coins.model.enums.ErrorType
import br.com.in8.coins.persistence.UserRepository
import br.com.in8.coins.preferences.EntitiesPreferences
import br.com.in8.coins.servives.listeners.LoginView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.util.*

@SuppressLint("CheckResult")
class UserService: KoinComponent, AnkoLogger {

    private val entitiesPreferences: EntitiesPreferences by inject()
    private val userRepository: UserRepository by lazy { UserRepository() }

    fun login(name: String, password: String, callback: LoginView) {
        Observable.fromCallable {
            val user = userRepository.findById(name, password)
            user?.also {
                entitiesPreferences.saveUser(CoinApp.instance, it)
            } ?: run {
                val u = User(UUID.randomUUID().toString(), name, password)
                userRepository.save(u)
                entitiesPreferences.saveUser(CoinApp.instance, u)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                callback.onSuccess()
            }, {
                error(it.message, it)
                callback.onError(ErrorType.DEFAULT)
            })
    }

}