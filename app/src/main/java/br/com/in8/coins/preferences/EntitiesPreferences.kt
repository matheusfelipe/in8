package br.com.in8.coins.preferences

import android.content.Context
import br.com.in8.coins.model.User
import br.com.in8.coins.utils.JsonUtils

class EntitiesPreferences {

    companion object {
        private const val KEY_APP_USER = "KEY_USER"
    }

    //region ~~~~ Application User ~~~~
    fun saveUser(context: Context, appConfig: User) =
            save(context, KEY_APP_USER, JsonUtils.defaultFormatter.toJson(appConfig))

    fun appUser(context: Context): User {
        val result = fetchString(context, KEY_APP_USER)
        if (result.isNullOrEmpty()) return User()

        return JsonUtils.defaultFormatter.fromJson(result, User::class.java)
    }

    fun cleanUser(context: Context) {
        save(context, KEY_APP_USER, "")
    }

    fun isCurrentUser(context: Context): Boolean {
        val currentUser = fetchString(context, KEY_APP_USER) ?: ""
        return currentUser.isNotEmpty()
    }
    //endregion

}
