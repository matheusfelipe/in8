package br.com.in8.coins.persistence

import androidx.test.internal.util.Checks.checkArgument
import br.com.in8.coins.model.Historic
import br.com.in8.coins.model.Historic_Table

class HistoricRepository : RepositoryBase<Historic>(Historic::class.java) {

    override fun isValid(model: Historic) {
        checkArgument(model.id.isNotEmpty())
        checkArgument(model.coinBefore.isNotEmpty())
        checkArgument(model.coinAfter.isNotEmpty())
    }

    fun findById(id: String): Historic? = find(Historic_Table.id.eq(id))

    fun findByUser(user: String): List<Historic> =
            findAll(Historic_Table.user.eq(user))

}
