package br.com.in8.coins.servives.rest

import br.com.in8.coins.model.Coin
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface CoinRest {

    @GET("latest/")
    fun getCoins(@QueryMap options: Map<String,String>): Observable<Coin>

}