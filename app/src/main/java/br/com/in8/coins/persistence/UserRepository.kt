package br.com.in8.coins.persistence

import androidx.test.internal.util.Checks
import br.com.in8.coins.model.User
import br.com.in8.coins.model.User_Table

class UserRepository : RepositoryBase<User>(User::class.java) {

    override fun isValid(model: User) {
        Checks.checkArgument(model.id.isNotEmpty())
        Checks.checkArgument(model.name.isNotEmpty())
        Checks.checkArgument(model.password.isNotEmpty())
    }

    fun findById(name: String, password: String): User? =
        find(User_Table.name.eq(name), User_Table.password.eq(password))


}
