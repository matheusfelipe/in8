package br.com.in8.coins.di

import br.com.in8.coins.preferences.EntitiesPreferences
import org.koin.dsl.module.module

val uiModule = module {
    single { EntitiesPreferences() }
}
