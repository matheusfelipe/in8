package br.com.in8.coins.viewmodels.views

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import br.com.in8.coins.adapters.CoinTypeAdapter
import br.com.in8.coins.viewmodels.items.CoinTypeItemViewModel
import org.koin.standalone.KoinComponent

class FindCoinViewModel(
) : ViewModel(), KoinComponent {

    private var selectedCoinType: CoinTypeItemViewModel? = null
    val openEnabled = ObservableBoolean(false)

    private val list = mutableListOf("CAD","HKD","ISK","PHP","DKK","HUF",
        "CZK","AUD","RON","SEK","IDR","INR","BRL","RUB","HRK","JPY","THB","CHF","SGD",
        "PLN","BGN","TRY","CNY","NOK","NZD","ZAR","USD","MXN","ILS","GBP","KRW","MYR")

    private lateinit var adapter: CoinTypeAdapter

    fun search(adapter: CoinTypeAdapter) {
        this.adapter = adapter
        adapter.updateItems(list.map { CoinTypeItemViewModel.fromCoinType(it) })
    }

    fun selectItem(item: CoinTypeItemViewModel, position: Int) {
        selectedCoinType = item
        item.isCheck = true

        adapter.updateSelectedItem(item, position)
        openEnabled.set(true)
    }

    //region ~~~~ Factory ~~~~
    class Factory(
        private val adapter: CoinTypeAdapter
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = FindCoinViewModel() as T
    }
    //endregion
}